# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueue():

    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    def dequeue(self):
        assert self.head

        _value = self.head.value
        self.head = self.head.link

        if self.head is None:
            self.tail = None

        return _value

    def enqueue(self, value):
        n = LinkedQueueNode(value)

        if self.head is None:

            self.head = n
            self.tail = n

        else:
            self.tail.link = n
            self.tail = n

        return None


class LinkedQueueNode():

    def __init__(self, value, link=None):
        self.value = value
        self.link = link


queue = LinkedQueue()
